def translate(words)
  translated_words = words.split(" ").map do |word|
    translate(word)
  end
  translated_words.join(" ")
end

def translate(word)
  $vowels = %w{a e i o u}
  if $vowels.include?(word[0])
    "#{word}ay"
  else
    phoneme = 0
    until $vowels.include?(word[phoneme])
      phoneme += 1
    end
    phoneme += 1 if word[phoneme - 1] == "q"
    "#{word[phoneme..-1]}#{word[0...phoneme]}ay"
  end
end
