def add(a, b)
  return a + b
end

def subtract(a, b)
  return a - b
end

def sum(addends)
  return (addends).reduce(0, :+)
end

def multiply(multiplicands)
  return (multiplicands).reduce(0, :*)
end
