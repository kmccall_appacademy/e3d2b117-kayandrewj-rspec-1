def ftoc(fahrenheit)
  return (fahrenheit - 32)*(5.0/9)
end

def ctof(celsius)
  return (celsius*(9.0/5)) + 32
end
