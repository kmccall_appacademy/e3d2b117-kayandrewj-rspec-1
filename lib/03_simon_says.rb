def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, repeater = 2)
  repeated = Array.new(repeater, word)
  repeated.join(" ")
end

def start_of_word(word, segment = 1)
  stop_at = segment - 1
  word[(0..stop_at)]
end

def first_word(phrase)
  phrase.split.first
end

def titleize(phrase)
  phrase = phrase.capitalize
  little_words = ["over", "and", "the"]
  phrase = phrase.gsub(/\w+/).each do |word|
    !little_words.include?(word) ? word.capitalize : word
  end
end
